﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Web.Administration;
using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace IIS.SitePuller
{
    class Program
    {

        private static string _AWSKey = Properties.Settings.Default.KEY;
        private static string _AWSSecret = Properties.Settings.Default.SECRET;
        private static string _Bucket = "server-sites";
        private static string _MainIP = GetMainIP();

        static void Main(string[] args)
        {
            using (IAmazonS3 s3Client = new AmazonS3Client(_AWSKey, _AWSSecret, RegionEndpoint.USEast1))
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    var _csvString = "SiteID,Name,MainDomain,OtherDomains,Active" + Environment.NewLine;
                    try
                    {
                        var sites = serverManager.Sites;
                        var siteCt = sites.Count;
                        for (int i = 0; i < siteCt; ++i)
                        {
                            var _bindings = sites[i].Bindings;
                            var adCt = _bindings.Count;
                            var _bList = "";
                            for (int j = 0; j < _bindings.Count; ++j)
                            {
                                _bList += (j > 0) ? _bindings[j].Host + "|" : "";
                            }
                            _csvString += CheckIsNull<long>(sites[i].Id, 0) + "," + CheckIsNull<string>(sites[i].Name, "") + "," + CheckIsNull<string>(_bindings[0].Host, "") + "," + CheckIsNull<string>(_bList, "") + "," + CheckIsNull<ObjectState>(sites[i].State, ObjectState.Unknown) + Environment.NewLine;
                        }
                    }
                    catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                    using (StreamWriter _sw = new StreamWriter(@_MainIP + "-sites.csv"))
                    {
                        _sw.Write(_csvString);
                        _sw.Close();
                    }
                }
                PutFile(s3Client);
            }
        }

        private static void PutFile(IAmazonS3 client)
        {
            try
                {
                    PutObjectRequest _putFile = new PutObjectRequest { 
                        BucketName = _Bucket,
                        ContentType = "text/csv",
                        FilePath = @_MainIP + "-sites.csv"
                    };
                    client.PutObject(_putFile);
                } 
                catch (AmazonS3Exception s3Ex)
                {
                    if (s3Ex.ErrorCode != null &&  (s3Ex.ErrorCode.Equals("InvalidAccessKeyId") || s3Ex.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Console.WriteLine("Check the provided AWS Credentials.");
                        Console.WriteLine("For service sign up go to http://aws.amazon.com/s3");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("Error occurred. Message:'{0}' when writing an object", s3Ex.Message);
                        Console.ReadLine();
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error occurred. Message:'{0}' when writing an object", ex.Message);
                    Console.ReadLine();
                }
        }

        private static string GetMainIP()
        {
            try {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }

        private static T CheckIsNull<T>(T Value, T Default)
        {
            T _Ret = Value == null || string.IsNullOrWhiteSpace(Value.ToString()) || ((object.ReferenceEquals(Value.GetType(), typeof(int)) || object.ReferenceEquals(Value.GetType(), typeof(decimal)) || object.ReferenceEquals(Value.GetType(), typeof(double)) || object.ReferenceEquals(Value.GetType(), typeof(long)) || object.ReferenceEquals(Value.GetType(), typeof(float)) || object.ReferenceEquals(Value.GetType(), typeof(short)) || object.ReferenceEquals(Value.GetType(), typeof(byte)))) ? (T)Default : (T)Value;
            return _Ret;
        }
    }
}
